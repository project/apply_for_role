This module is currently at an 8.x-1.0 alpha state.

POST ALPHA LAUNCH REMAINING ITEMS:
-Make apply for role applications administration page have filtering functionality.
-Alter registration form to include apply for role (conditional per config)
-Create tab on user profile to allow a user to see a user's submitted applications (conditional per config).
-Modify apply for role form to only display roles that have do not currently have applications pending for them with current user.
